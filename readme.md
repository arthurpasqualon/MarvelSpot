# 🦸‍♀️ MarvelSpot App 
## 📚 About the project


MarvelSpot was born to provide an easy way to retrieve and search your favorite comics.
It is a Mobile solution, developed in React Native wich communicates with Marvel's Comics collection API.

  ## 🌐  Technologies

-   [React Native](https://reactnative.dev/)
-   [Yarn](https://yarnpkg.com/)
-   [Typescript](https://www.typescriptlang.org)
-   [Jest](https://jestjs.io)

## ✔️ Resources

  
+ [Check Figma's project link](https://www.figma.com/file/OB1kweqCeHAhvru7QHoIE8/Marvel?node-id=5%3A2)

+ [Download the Android version](https://install.appcenter.ms/users/arthur.pasqualon/apps/marvelspot-1/distribution_groups/public%20link)

+ [Download the IOS version (Adhoc only)](https://install.appcenter.ms/users/arthur.pasqualon/apps/marvelspot/distribution_groups/public%20link)

  

##  📱Features

+ Onboarding.

![enter image description here](https://mixed-arthurpasqualon.s3.amazonaws.com/Captura+de+Tela+2021-03-16+a%CC%80s+21.30.33.png)

+ Select Character.

![enter image description here](https://mixed-arthurpasqualon.s3.amazonaws.com/Captura+de+Tela+2021-03-16+a%CC%80s+21.30.52.png)

+ Search Characters.

![enter image description here](https://mixed-arthurpasqualon.s3.amazonaws.com/Captura+de+Tela+2021-03-16+a%CC%80s+21.30.59.png)

+ Explore Character's Comics.

![enter image description here](https://mixed-arthurpasqualon.s3.amazonaws.com/Captura+de+Tela+2021-03-16+a%CC%80s+21.31.21.png)

+ Check the Comic's price, issue number, cover and title.

![enter image description here](https://mixed-arthurpasqualon.s3.amazonaws.com/Captura+de+Tela+2021-03-16+a%CC%80s+21.31.25.png)

+ Check Comic's details on the Marvel website.

+ Explore nearby bookstores.

+ Add Comic's to your favorite list (Soon).

  
  

## 💯 Installation

  
Asuming that you already have the basic react native setup on your machine.

  

+ ```git clone ${{repo}}```

+ ```yarn ```

+ ```cd ios && pod install ```

+ create .env file in the root of the project and add the following variables:

	+ ``` API_URL=https://gateway.marvel.com:443/v1/public/ ```

	+ ``` API_AUTH=ts=${YOUR_TIMESTAMP}&apikey=${YOUR_API_KEY}&hash=${YOUR_HASH} ```

	+ ``` MAPS_KEY=${YOUR_GOOGLE_MAPS_KEY} ```
+ run the app:

+ ``` react-native run-android ``` or ``` react native run-ios ```